package com.bank.mega.config;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.util.Strings;
import org.springframework.web.bind.annotation.RequestMethod;

public class AuthenticationStringEncoder {
	
	public static String getSecretHmacEncriptor(String secretKey,
		   String secretId, RequestMethod secretMethod, String secretEndpoints,String secretUrl, String secretRequestbody) {
		   try {
			     String secret = secretKey;
			     String nonce = getSecretNonce();
			     String timestamp = getSecretUnixNowTimeStamp().toString();
			     String id = secretId;

			        String message = id
			    		           + getSecretHttpMethod(secretMethod)
			                       + getSecretUrl(secretEndpoints,secretUrl)
			    		           + timestamp
			    		           + nonce
			    		           + getSecretRequestBody(secretRequestbody);
			   
			     Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
			     SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(), "HmacSHA256");
			     sha256_HMAC.init(secret_key);

			     String hash = Base64.encodeBase64String(sha256_HMAC.doFinal(message.getBytes()));
			     hash = id+":"+hash+":"+nonce+":"+timestamp;
			     String resultHash = Base64.encodeBase64String(hash.getBytes());
			     return resultHash;
			    }
			    catch (Exception e){
			     System.out.println("Error");
			     return null;
			    }
	}  
	
	private static String getSecretUrl(String endPoints,String url) throws UnsupportedEncodingException {
		String finalUrl = URLEncoder.encode(endPoints+url, StandardCharsets.UTF_8.toString()).toLowerCase(); 
		System.out.println("{ \"url\" : " +endPoints+url+ " \n " 
		         + " \"final url\" : " + finalUrl + " }");
		return finalUrl;
	}
	
	private static String getSecretHttpMethod(RequestMethod method) {
		String finalMethod = method.name().toUpperCase();
		 System.out.println("{ \"method\" : " +method.name()+ " \n " 
			         + " \"final method\" : " + finalMethod + " }");
		return finalMethod;
	}
	
	private static String getSecretNonce() {
		UUID uuid = UUID.randomUUID();
		String finalUuid = uuid.toString().replace("-", "");
		System.err.println("{\"finalUuid\" : " + finalUuid);
		return finalUuid;
	}
	
	private static Long getSecretUnixNowTimeStamp() {
		long unixTime = System.currentTimeMillis() / 1000L;
		System.err.println("{\"unixTime\" : " + unixTime);
		return unixTime;
	}
	
	private static String getSecretRequestBody(String requestBody) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		if(Strings.isBlank(requestBody)) {
			String finalDigest = "nUVowAnSA6sQ4z6plToCZA==";
			 System.out.println("{ \"request body\" : " +requestBody+ " \n " 
			         + " \"final request body\" : " + finalDigest + " }");
			return finalDigest; 
		}
		requestBody = removalUneededChar(requestBody, " ","\n","\r","\t");
	        MessageDigest m = MessageDigest.getInstance("MD5");
	        byte[] digest = m.digest(requestBody.getBytes("UTF-8"));
	        System.err.println("byte : " + digest);
	        String finalDigest =  Base64.encodeBase64String(digest);
	        System.out.println("{ \"request body\" : " +requestBody+ " \n " 
	         + " \"final request body\" : " + finalDigest + " }");
		return finalDigest;
	}
	
	private static String removalUneededChar(String word,String... chars) {
		for (String ch : chars) {
			word = word.replaceAll(ch, "");
		}
		return word;
	}
}
