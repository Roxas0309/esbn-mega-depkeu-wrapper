package com.bank.mega.controller.esbnCtl;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.util.Strings;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bank.mega.config.AuthenticationStringEncoder;
import com.bank.mega.config.HttpRestResponse;
import com.google.gson.Gson;


@RestController
@RequestMapping("/esbn-connector")
public class EsbnConnectorWebService extends BaseCtl{

    @PostMapping("/call-url/with-body")
	public String mapperConnectorEsbn(
			 @RequestHeader(name = "secret-url", required = true)String secretUrl,
    		 @RequestHeader(name = "secret-method", required = true)String secretMethod,
    		 @RequestBody Map<String, Object> body){
		Map<String, Object> map = new HashMap<>();
		String secretBody = new Gson().toJson(body);
		RequestMethod requestMethod = null;
		HttpMethod httpMethod = null;
		if(secretMethod.equalsIgnoreCase("POST")) {
			requestMethod = RequestMethod.POST;
			httpMethod = HttpMethod.POST;
		}
		else if(secretMethod.equalsIgnoreCase("PUT")) {
			requestMethod = RequestMethod.PUT;
			httpMethod = HttpMethod.PUT;
		}
		String tokenHmac = AuthenticationStringEncoder.getSecretHmacEncriptor
				(DEPKEU_API_KEY, DEPKEU_API_ID, requestMethod, 
						DEPKEU_URL_API, secretUrl, secretBody);
		String url = DEPKEU_URL_API + secretUrl.trim();
		System.out.println("final url depkeu : " + url);
		System.out.println("my token : " +  "amx " + tokenHmac);
		Map<String, String> headerMap = new HashMap<>();
		headerMap.put("Authorization", "amx " + tokenHmac);
		HttpRestResponse httpRestResponse = wsBodyEsbn
				(url, body, httpMethod, headerMap);
		return httpRestResponse.getBody();
	}
    
    @GetMapping("/call-url/no-body")
  	public String mapperConnectorEsbn(
  			 @RequestHeader(name = "secret-url", required = true)String secretUrl,
      		 @RequestHeader(name = "secret-method", required = true)String secretMethod){
  		Map<String, Object> map = new HashMap<>();
  		RequestMethod requestMethod = null;
  		HttpMethod httpMethod = null;
  		if(secretMethod.equalsIgnoreCase("GET")) {
  			requestMethod = RequestMethod.GET;
  			httpMethod = HttpMethod.GET;
  		}
  		else if(secretMethod.equalsIgnoreCase("DELETE")) {
  			requestMethod = RequestMethod.DELETE;
  			httpMethod = HttpMethod.DELETE;
  		}
  		String tokenHmac = AuthenticationStringEncoder.getSecretHmacEncriptor
  				(DEPKEU_API_KEY, DEPKEU_API_ID, requestMethod, 
  						DEPKEU_URL_API, secretUrl, "");
  		String url = DEPKEU_URL_API + secretUrl.trim();
  		System.out.println("final url depkeu : " + url);
  		System.out.println("my token : " +  "amx " + tokenHmac);
  		Map<String, String> headerMap = new HashMap<>();
  		headerMap.put("Authorization", "amx " + tokenHmac);
  		HttpRestResponse httpRestResponse = wsBodyEsbn
  				(url, null, httpMethod, headerMap);
  		return httpRestResponse.getBody();
  	}
	
}
